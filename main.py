#!/usr/bin/env python

import numpy as np

from ConfigParser import SafeConfigParser
import os
import sys

def read_config(filename):
	parser = SafeConfigParser()
	parser.read(filename)
	return parser

if __name__ == '__main__':
	if len(sys.argv) == 1:
		filename = 'config.ini'
	else:
		filename = sys.argv[1]
	parser = read_config(filename)

	if parser.getboolean('process', 'gen_cov'):
		### generate_covariance
		import gen_cov.generate_ylm as gylm
		import gen_cov.generate_pl_mp as gpl_mp
		import gen_cov.generate_cov as gcov

		nside = parser.getint('convert', 'nside_out')
		l_max = parser.getint('convert', 'l_max')
		ylm_name = parser.get('convert', 'ylm_name').replace('[nside]',str(nside))
		pl_name = parser.get('convert', 'pl_name').replace('[nside]',str(nside))
		cov_name = parser.get('convert', 'cov_name').replace('[nside]',str(nside))
		print "nside = ", nside
		print "l_max = ", l_max
		gylm.main(nside, l_max, ylm_name)
		gpl_mp.main(nside, l_max, ylm_name, pl_name)

		cwd = os.path.dirname(os.path.abspath(__file__))
		sig = cwd + '/' + parser.get('map_in', 'signal_filename')
		sca = cwd + '/' + parser.get('map_in', 'scalar_filename')
		lens = cwd + '/' + parser.get('map_in', 'lens_filename')
		print sig, sca, lens
		nside_in = parser.getint('map_in', 'nside_in')
		l_max_in = parser.getint('map_in', 'l_max_in')
		smooth_factor = parser.getfloat('convert', 'smooth_factor')
		useEB = parser.getboolean('map_in', 'useEB')
		gcov.main(sig, sca, lens, nside_in, l_max_in, nside, l_max, smooth_factor, pl_name, cov_name, useEB)

	if parser.getboolean('process', 'convert_map'):
		### convert maps
		import mapconv.conv as conv
		params = conv.para(parser)
		conv.main(params)

	if parser.getboolean('process', 'fit'):
		import dmm.fit as fit
		import dmm.show as show
		fit.main(parser)
		if parser.getboolean('process', 'show'):
			#show.hist("dmm/"+parser.get('output','out_name'))
			show.hist([parser.get('output','out_name')])
			

