#!/usr/bin/env python
import os
import numpy as np
import healpy as hp

class para(object):
	def __init__(self, parser=None):
		if parser is None:
			self.set_DEFALT()
		else:
			self.read_config(parser)
		self.calc_params()
	
	def read_config(self, parser):
		self.Nside_obs = parser.getint('map_in', 'nside_in')
		self.nu_list = np.array([float(i) for i in parser.get('map_in','nu_list').split()])
		if parser.getboolean('map_in','smooth_in'):
			self.beam = np.array([float(i) for i in parser.get('map_in','beam').split()])
		else:
			self.beam    = np.zeros(15)

		self.Nside = parser.getint('convert', 'nside_out')
		self.smooth_factor = parser.getfloat('convert', 'smooth_factor')

		self.name = parser.get('map_in', 'map_name')
		self.prefix = parser.get('convert', 'conv_prefix')
		self.out_name = ""
		self.repeat_num = parser.getint('map_in', 'num_max')

	def set_DEFALT(self):
		self.Nside_obs = 128
		self.nu_list = np.array([40.,50.,60.,68.,78.,89.,100.,119.,140.,166.,195.,235.,280.,337.,402.])
		self.beam    = np.zeros(15)
		self.Nside = 4
		self.smooth_factor = 2.5
		### first: freq. second: number
		self.name = "../gm100/output/mapPLPL_%d_%d.fits"
		self.prefix = "conv"
		self.out_name = "./new_maps/"+self.prefix+"_mapPLPL_AME_%d_%d" + "_%d.fits"%self.Nside
		self.repeat_num = 100
	
	def calc_params(self):
		self.Npix_obs = 12 * self.Nside_obs**2
		self.Npix = 12 * self.Nside**2
	
		arcmin2rad = np.pi / (180.0 * 60.0)
		self.beam_in_radian = self.beam * arcmin2rad

	def set_out_name(self, dir_name):
		import os
		if not os.path.isdir(dir_name):
			os.mkdir(dir_name)
		filename = os.path.basename(self.name)
		self.out_name = dir_name + '/' + self.prefix + filename

def name_replace(name, nu, num):
	new_name = name.replace('[nu]', str(int(nu))).replace('[num]', str(num))
	return new_name

### 2.5 * pixel_size smoothing
def compute_smoothing_beam(Nside, smooth_factor):
	Npix = 12 * Nside**2
	delta_omega = 4.0 * np.pi / Npix
	pix_da = np.sqrt(delta_omega)
	beam_size = pix_da * smooth_factor
	beam = np.array(hp.gauss_beam(fwhm=beam_size, lmax=2*Nside, pol=True))
	return beam

### beam window function and pixel window function to alm
def smooth_and_downgrade(m, Nside_in, Nside_out, beam_in_radian, smooth_factor):
	if Nside_in == Nside_out:#yminami
		m = hp.reorder(m, r2n=True)
		return m
	l_max = 2 * Nside_out
	alm = hp.map2alm(m, lmax=l_max, iter=5)
	
	pw_in = hp.pixwin(Nside_in, pol=True)
	pw_out = hp.pixwin(Nside_out, pol=True)

	beam = np.array(hp.gauss_beam(fwhm=beam_in_radian, lmax=l_max, pol=True))
	smoothing_beam = compute_smoothing_beam(Nside_out, smooth_factor)

	fact = np.zeros((l_max+1), dtype=np.float64)
	for l in xrange(l_max+1):
		if pw_in[1][l] * beam[l][1] > 0.0:
			fact[l] = smoothing_beam[l][1] * pw_out[1][l] / (pw_in[1][l] * beam[l][1])
		else:
			pass
	hp.almxfl(alm[1], fact, inplace=True)
	hp.almxfl(alm[2], fact, inplace=True)

	m = hp.alm2map(alm, Nside_out, lmax=l_max, pixwin=False, verbose=False)
	m = hp.reorder(m, r2n=True)
	return m

def run(a):
	i = a[0]
	con = a[1]
	
	for nu, fwhm in zip(con.nu_list, con.beam_in_radian):
		if os.path.exists(name_replace(con.out_name, nu, i+1)):
			continue
		print "--------",i+1,"--------",nu,' GHz'
		maps = hp.read_map(name_replace(con.name, nu, i+1), (0,1,2), verbose=False)
		maps = smooth_and_downgrade(maps, con.Nside_obs, con.Nside, fwhm, con.smooth_factor)
		hp.write_map(name_replace(con.out_name, nu, i+1), maps, nest = True)



def main(con=None):
	if con is None:
		con = para()
	
	con.set_out_name('output/converted_map')
	xx = con.repeat_num

	func_args = []
	for x in range(xx):
		func_args.append((x, con))

	## not use multiprocessing
	#for f in func_args:
		#run(f)

	# use multiprocessing
	import multiprocessing as mp
	pool = mp.Pool(8)
	#pool = mp.Pool(mp.cpu_count()/2)
	callback = pool.map(run, func_args)

	print "convert complete!"



if __name__ == '__main__':
	main()
	#for i in range(x):
	#	run(i)

