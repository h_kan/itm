import multiprocessing as mp
import healpy as hp
import numpy as np
import os

pi = np.pi


def generate_plxy2(nside, l_max, ylm2, l):
	npix = hp.nside2npix(nside)
	offY = 0
	plxy = np.zeros((3, npix, npix), dtype=np.float64)
	idx = hp.Alm.getidx(l_max, l, 0)
	y = ylm2[0, 0, idx]
	w = ylm2[1, 1, idx]
	x = ylm2[1, 2, idx]
	yc = np.conj(y)
	wc = np.conj(w)
	xc = np.conj(x)
	plxy[offY] = (np.outer(w, wc)).real
	plxy[offY+1] = (np.outer(w, xc)).real
	plxy[offY+2] = (np.outer(x, xc)).real
	if l>0:
		for m in xrange(1, l+1):
			idx = hp.Alm.getidx(l_max, l, m)
			y = ylm2[0, 0, idx]
			w = ylm2[1, 1, idx]
			x = ylm2[1, 2, idx]
			yc = np.conj(y)
			wc = np.conj(w)
			xc = np.conj(x)
			plxy[offY] += (np.outer(w, wc) + np.outer(wc, w)).real 
			plxy[offY+1] += (np.outer(w, xc) + np.outer(wc, x)).real
			plxy[offY+2] += (np.outer(x, xc) + np.outer(xc, x)).real
	return plxy

def generate_pl2(nside, l_max, plxy, l):
	npix = hp.nside2npix(nside)
	#for P_l^{EE} and P_l^{BB} for Q/U
	#pl = np.zeros((2, npix*2, npix*2), dtype=np.float64)
	pl = np.zeros((3, npix*2, npix*2), dtype=np.float64)
	###pl[0] is for EE, pl[1] is for BB, pl[2] is for EB
	for ip in xrange(npix):
		ip_ring = hp.nest2ring(nside, ip)
		for jp in xrange(npix):
			jp_ring = hp.nest2ring(nside, jp)
			pl[0, ip, jp] = plxy[0, ip_ring, jp_ring] ##w*wc 
			pl[0, ip, jp+npix] = -plxy[1, ip_ring, jp_ring] ##- w*xc 
			pl[0, ip+npix, jp] = -plxy[1, jp_ring, ip_ring] ##- x*wc
			pl[0, ip+npix, jp+npix] = plxy[2, ip_ring, jp_ring] ##+ x*xc

			pl[1, ip, jp] = plxy[2, ip_ring, jp_ring]
			pl[1, ip, jp+npix] = plxy[1, jp_ring, ip_ring]
			pl[1, ip+npix, jp] = plxy[1, ip_ring, jp_ring]
			pl[1, ip+npix, jp+npix] = plxy[0, ip_ring, jp_ring]

			pl[2, ip, jp] =( plxy[1, ip_ring, jp_ring]*2. ) ## w*xc+x*wc
			pl[2, ip, jp+npix] = (plxy[0, jp_ring, ip_ring]  - plxy[2, jp_ring, ip_ring])  ##+ w*wc-x*xc
			pl[2, ip+npix, jp] = (-plxy[2, ip_ring, jp_ring] + plxy[0, ip_ring, jp_ring] )  ##- x*xc+ w*wc
			pl[2, ip+npix, jp+npix] = -(plxy[1, ip_ring, jp_ring]*2 ) ##-x*wc - w*xc

	return pl

def doit(a):
	nside = a[0]
	l_max = a[1]
	ylm2 = a[2]
	l = a[3]
	name = a[4]

	print 'l=',l

	plxy = generate_plxy2(nside, l_max, ylm2, l)
	pl = generate_pl2(nside, l_max, plxy, l)
	#print pl.shape
	print 'yminami print ', 'pl/'+name.replace('[l]',str(l))
	print pl
	print l
	#np.savez('pl/'+name.replace('[l]',str(l)), pl2=pl, l=l)
	np.savez('output/pl/'+name.replace('[l]',str(l)), pl2=pl, l=l)
	#np.savez('pl/pl2n%d-l%02d.npz'%(nside, l), pl2=pl, l=l)
	return

def main(nside, l_max, name_in, name):
	ylm2 = np.load('output/ylm/'+name_in)['ylm2']
	if not os.path.isdir('output/pl'):
		os.mkdir('output/pl')

	pool = mp.Pool(mp.cpu_count())
	func_args = []
	for l in xrange(l_max+1):
		func_args.append((nside, l_max, ylm2, l, name))
	pool.map(doit, func_args)

	print("generate pl complete")

if __name__ == '__main__':
	nside = 4
	l_max = 2*nside
	main(nside, l_max)

	

	
