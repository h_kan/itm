import numpy as np
import healpy as hp

import os

pi = np.pi

def generate_ylm2(nside, l_max=None):
	if l_max is None:
		#l_max = 3*nside-1
		l_max = 2*nside
	npix = hp.nside2npix(nside)
	map_ = np.zeros([3,npix])
		
	nterms = ((l_max+1)*(l_max+2))/2

	# one over omega_pix
	one_oop = npix/(pi*4.0)

	# output ylmc
	ylmc = np.zeros((3, 3, nterms, npix), dtype=np.complex128)

	# T
	for ipx in xrange(npix):
		map_[0][ipx] = one_oop
		alm = hp.map2alm(map_, lmax = l_max, iter=0, use_weights=False)
		ylmc[0, 0, :, ipx] = alm[0]
		map_[0][ipx] = 0.0

	# Q
	for ipx in xrange(npix):
		map_[1][ipx] = one_oop
		alm = hp.map2alm(map_, lmax = l_max, iter=0, use_weights=False)
		ylmc[1, 1, :, ipx] = alm[1]
		ylmc[1, 2, :, ipx] = alm[2]
		map_[1][ipx] = 0.0

	# U
	for ipx in xrange(npix):
		map_[2][ipx] = one_oop
		alm = hp.map2alm(map_, lmax = l_max, iter=0, use_weights=False)
		ylmc[2, 1, :, ipx] = alm[1]
		ylmc[2, 2, :, ipx] = alm[2]
		map_[2][ipx] = 0.0

	ylm = np.conjugate(ylmc)
	return ylm

def main(nside, l_max, name):
	#out_dir = 'output/ylm'
	out_dir = 'output/ylm/'
	if not os.path.isdir(out_dir):
		os.mkdir(out_dir)
	ylm = generate_ylm2(nside, l_max)
	np.savez(out_dir+name, ylm2=ylm)
	print "generate ylm complete"

if __name__ == '__main__':
	nside = 4
	l_max = 2*nside

	main(nside, l_max)
