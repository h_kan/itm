import healpy as hp
import numpy as np

import os

sigma2fwhm = np.sqrt(8.0 * np.log(2.0))
rad2arcmin = 180.0 * 60.0 / np.pi
arcmin2rad = 1.0 / rad2arcmin

def beam_in_rad(beam_size):
	# unit of input_beam is arcmin
	# output is radian
	beam_in_radian = beam_size * arcmin2rad
	return beam_in_radian

def read_cls(filename, lmax):
	print filename
	#filename = "./gm100/input/cmb_cls/planck15lens_tensCls.dat"
	cl = np.loadtxt(filename)
	if len(cl[0]) in (4, 6): 
		cl = np.c_[cl[:,:3], np.zeros(len(cl))[:,np.newaxis],cl[:,3]]
	cl, l = cl.T[1:5], cl.T[0]
	cl = cl * 2*np.pi/(l*(l+1))
	l_start = int(l[0])
	l = np.r_[np.arange(l_start), np.array(map(int, l))]
	cl = np.c_[np.zeros(4*l_start).reshape(4,l_start), cl]
	if lmax > l[-1]:
		raise ValueError("too large lamx is set")
	else:
		#i_lmax = np.where(l == lmax)[0][0]
		i_lmax = lmax + 1
		l = l[:i_lmax]
		cl = cl[:,:i_lmax]
	return cl


### fact(0) : TT (spin=0)
### fact(1) : EE (spin=2)
### fact(2) : BB (spin=2) the same as EE
### fact(0) : TE (spin=1)

def beam_me_up2(cls, beam, pol=True):
	fact = np.array(hp.gauss_beam(fwhm=beam_in_rad(beam), lmax=len(cls[0])-1, pol=pol)).T
	cls = cls/fact**2
	return cls

#def beam_me_up2(cls, beam, pol=True):
#	fact = np.array(hp.gauss_beam(fwhm=beam_in_rad(beam), lmax=len(cls[0])-1))
#	if pol:
#		cls = cls / fact
#		#cls = cls / fact[1]
#	else:
#		cls = cls / fact[0]
#	return cls

def beam_me_down2(cls, beam, pol=True):
	fact = np.array(hp.gauss_beam(fwhm=beam_in_rad(beam), lmax=len(cls[0])-1, pol=pol)).T
	cls = cls*fact**2
	return cls

#def beam_me_down2(cls, beam, pol=True):
#	beam = (beam_in_rad(beam)/sigma2fwhm)**2
#	if pol:
#		for l in xrange(cls.shape[1]):
#			#cls[:,l] *= np.exp(-0.5(l*(l+1.0)-4)*beam)
#			cls[:,l] *= np.exp(-(l*(l+1.0)-4)*beam)
#	else:
#		for l in xrange(cls.shape[1]):
#			#cls[:,l] *= np.exp(-l*(l+1.0)*beam) ### same as Nkpy
#			cls[:,l] *= np.exp(-0.5*l*(l+1.0)*beam)
#	return cls

def pixwin_up2(cls, nside, pol=True):
	pixwin = hp.pixwin(nside, True)
	
	if pol:
		pixwin_up2b(cls[0], pixwin[0], pixwin[0])  # TT
		pixwin_up2b(cls[1], pixwin[1], pixwin[1])  # EE
		pixwin_up2b(cls[2], pixwin[1], pixwin[1])  # BB 
		pixwin_up2b(cls[3], pixwin[0], pixwin[1])  # TE
	else:
		pixwin_up2b(cls[0], pixwin[0], pixwin[0])  # TT
		pixwin_up2b(cls[1], pixwin[0], pixwin[0])  # EE
		pixwin_up2b(cls[2], pixwin[0], pixwin[0])  # BB 
		pixwin_up2b(cls[3], pixwin[0], pixwin[0])  # TE

def pixwin_up2b(cls, pixwin1, pixwin2):
	for l in range(min(cls.shape[0], len(pixwin1))):
		cls[l] *= (pixwin1[l]*pixwin2[l])

def pixwin_down2(cls, nside, pol=True):
	pixwin = hp.pixwin(nside, True)

	if pol:
		pixwin_down2b(cls[0], pixwin[0], pixwin[0])  # TT
		pixwin_down2b(cls[3], pixwin[0], pixwin[1])  # TE
		pixwin_down2b(cls[1], pixwin[1], pixwin[1])  # EE
		pixwin_down2b(cls[2], pixwin[1], pixwin[1])  # BB
	else:
		pixwin_down2b(cls[0], pixwin[0], pixwin[0])  # TT
		pixwin_down2b(cls[3], pixwin[0], pixwin[0])  # TE
		pixwin_down2b(cls[1], pixwin[0], pixwin[0])  # EE
		pixwin_down2b(cls[2], pixwin[0], pixwin[0])  # BB

def pixwin_down2b(cls, pixwin1, pixwin2):
	for l in range(min(cls.shape[0], len(pixwin1))):
		if (pixwin1[l]*pixwin2[l])!=0.0:
			cls[l] /= (pixwin1[l]*pixwin2[l])


def compute_cov(signal_cls, ones_cls, l_max, nside, npix, scalar_cls, lens_cls, name_in, name, useEB=False):
	cov_EE = np.zeros((2*npix, 2*npix), dtype=np.float64)
	cov_BB = np.zeros((2*npix, 2*npix), dtype=np.float64)
	cov_ones = np.zeros((2*npix, 2*npix), dtype=np.float64)
	cov_scalar_EE = np.zeros((2*npix, 2*npix), dtype=np.float64)
	cov_lens_EE = np.zeros((2*npix, 2*npix), dtype=np.float64)
	cov_lens_BB = np.zeros((2*npix, 2*npix), dtype=np.float64)
	for l in xrange(l_max+1):
		print 'l=',l
		try:
			pl = np.load('output/pl/'+name_in.replace('[l]', str(l)))['pl2']

			cov_EE += signal_cls[1][l] * pl[0]
			cov_scalar_EE += scalar_cls[1][l] * pl[0]
			cov_lens_EE += lens_cls[1][l] * pl[0]
			cov_ones += ones_cls[1][l] * pl[0]

			cov_ones += ones_cls[2][l] * pl[1]
			cov_BB += signal_cls[2][l] * pl[1]
			cov_lens_BB += lens_cls[2][l] * pl[1]
			if useEB:
				cov_ones += ones_cls[3][l] * pl[2]
		except:
			raise ValueError('file for l=%d not found'%(l))
		#print np.average(cov_EE)
		#print np.average(cov_BB)
		#print np.average(cov_ones)
		#print np.average(cov_scalar_EE)
		#print np.average(cov_lens_EE)
		#print np.average(cov_lens_BB)

	#np.savez('cov/cov'+ 'n%d_p3nm1_c2n.npz'%nside, cov_EE=cov_EE, cov_BB=cov_BB, cov_noise=cov_ones, cov_scalar_EE=cov_scalar_EE, cov_lens_EE=cov_lens_EE, cov_lens_BB=cov_lens_BB)
	#np.savez('cov/'+name, cov_EE=cov_EE, cov_BB=cov_BB, cov_noise=cov_ones, cov_scalar_EE=cov_scalar_EE, cov_lens_EE=cov_lens_EE, cov_lens_BB=cov_lens_BB)
	np.savez('output/cov/'+name, cov_EE=cov_EE, cov_BB=cov_BB, cov_noise=cov_ones, cov_scalar_EE=cov_scalar_EE, cov_lens_EE=cov_lens_EE, cov_lens_BB=cov_lens_BB)

#def doit(signal_filename, scalar_filename, lens_filename, l_max_in, beam, nside, beam_in, nside_in):
def doit(signal_filename, scalar_filename, lens_filename, nside_in, l_max_in, nside, l_max, smooth_factor, name_in, name, useEB=False):
	npix = hp.nside2npix(nside)
	beam = np.sqrt(4*np.pi/npix) * rad2arcmin * smooth_factor
	beam_in = 0.

	# tensor 
	signal_cls = read_cls(signal_filename, lmax=l_max_in)
	signal_cls = beam_me_down2(signal_cls, beam)
	pixwin_up2(signal_cls, nside)

	# scalar
	scalar_cls = read_cls(scalar_filename, lmax=l_max_in)
	scalar_cls = beam_me_down2(scalar_cls, beam)
	pixwin_up2(scalar_cls, nside)

	# lens
	lens_cls = read_cls(lens_filename, lmax=l_max_in)
	lens_cls = beam_me_down2(lens_cls, beam)
	pixwin_up2(lens_cls, nside)

	# for noise
	ones_cls = np.ones((4,l_max+1), dtype=np.float64)

	ones_cls = beam_me_down2(ones_cls, beam)
	pixwin_up2(ones_cls, nside)
	ones_cls = beam_me_up2(ones_cls, beam_in)
	pixwin_down2(ones_cls, nside_in)

	compute_cov(signal_cls, ones_cls, l_max, nside, npix, scalar_cls, lens_cls, name_in, name, useEB)

def main(sig_name, sca_name, len_name, nside_in, l_max_in, nside, l_max, smooth_factor, name_in, name,useEB=False):
	if not os.path.isdir('output/cov'):
		os.mkdir('output/cov')
	doit(sig_name, sca_name, len_name, nside_in, l_max_in, nside, l_max, smooth_factor, name_in, name, useEB)
	print "generate cov complete"

	

if __name__ == "__main__":
	# TT, EE, BB, TE
	signal_filename = "../gm100/input/cmb_cls/planck15lens_tensCls.dat"
	scalar_filename = "../gm100/input/cmb_cls/planck15lens_scalCls.dat"
	lens_filename =   "../gm100/input/cmb_cls/planck15lens_lensedCls.dat"
	### IN params
	nside_in = 128
	l_max_in = nside_in*2

	### OUT params
	nside = 4
	l_max = nside*2
	smooth_factor = 2.5
	useEB=False
	pl_name = 'pl_n[nside]_l[l].npz'
	pl_name = pl_name.replace('[nside]',str(nside))
	name = 'cov_n[nside].npz'
	name = name.replace('[nside]',str(nside))
	
	main(signal_filename, scalar_filename, lens_filename, nside_in, l_max_in, nside, l_max, smooth_factor, pl_name, name, useEB)

