#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np

import sys


def hist(f):
	for i in range(len(f)):
		name = f[i]
		a = np.loadtxt(name,delimiter='\t')
		r = a.T[0]
	
	plt.hist(r, bins=30, histtype='stepfilled')
	plt.title('histogram', fontsize=17)
	plt.xlabel('$r_\mathrm{est}$', fontsize=18)
	plt.show()
	print "avg = ", np.average(r)
	print "std = ", np.std(r)


def out_hist(f, c, al, l, tit, r_input, x):
	for i in range(len(f)):
		name = f[i]
		a = np.loadtxt(name,delimiter='\t')
		r = a.T[0]
		print len(r)
		plt.hist(r, bins=30, histtype='stepfilled', color=c[i], alpha=al[i], label=l[i])
	
	plt.title(tit, fontsize=17)
	plt.xlabel('$r_\mathrm{est}$', fontsize=18)
	plt.axvline(x='%f'%r_input, color='black')
	plt.axvline(x=0.0, color='black')
	plt.xlim(x)
	plt.legend(numpoints=1)
	plt.savefig("%s.png"%tit)
	#plt.close()
	plt.show()



if __name__=='__main__':
	if len(sys.argv) > 1:
		f = sys.argv[1:]
		hist(f)

	else:
		#f = ['./data_modDMM_lense_n8_AME.txt', './data_modDDMM_lense_n8_AME.txt']
		#f = ['./data_cmbonly_n4_new.txt', './data_cmbonly_n8_new.txt']
		f = ['./data_cmbonly_n8_new.txt', './data_cmbonly_lense_n8_new.txt']
		color = ['blue', 'red']
		alpha = [0.8, 0.4]
		#label = ['$\mathrm{Nside=4}$', '$\mathrm{Nside=8}$']
		label = ['$\mathrm{w/\ lensing}$', '$\mathrm{w/o\ lensing}$']
		title = 'histgam of 100 simulations'
		r_input = 0.001
		x_lim = (0.00, 0.002)

		#out_hist(f)
		out_hist(f, color, alpha, label, title, r_input, x_lim)

