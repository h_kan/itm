#!/usr/bin/env python

from scipy.optimize import minimize
import matplotlib.pyplot as plt
import numpy as np
import healpy as hp

from ConfigParser import SafeConfigParser
import sys

import cov_mat as cv
import maps as mp

def read_config(filename):
	parser = SafeConfigParser()
	parser.read(filename)
	return parser

### main function
def main(parser):
	import os
	filepath = os.path.dirname(os.path.abspath(__file__))

	### paramters
	method = parser.get('method', 'method')
	Nside = parser.getint('convert', 'nside_out')
	art_fact = parser.getfloat('method', 'art_fact')
	seed = parser.getint('method', 'seed')

	#mask_name = filepath + '/mask_map/' + parser.get('method', 'mask_name')
	mask_name = filepath + '/mask_map/' + parser.get('method', 'mask_name')
	#cov_name = filepath + '/../cov/' + parser.get('convert', 'cov_name').replace('[nside]',str(Nside))
	cov_name = filepath + '/../output/cov/' + parser.get('convert', 'cov_name').replace('[nside]',str(Nside))
	#map_name = filepath + '/../converted_map/' + parser.get('convert', 'conv_prefix') + os.path.basename(parser.get('map_in', 'map_name'))
	map_name = filepath + '/../output/converted_map/' + parser.get('convert', 'conv_prefix') + os.path.basename(parser.get('map_in', 'map_name'))

	lense = parser.getboolean('method', 'lense')

	nu_list = np.array([float(i) for i in parser.get('method', 'nu_list').split()])
	bool_band = parser.getboolean('method', 'bool_band')
	if bool_band:
		bw_list = np.array([float(i) for i in parser.get('method', 'bw_list').split()])
	else:
		bw_list = np.zeros([len(nu_list)])
	bool_noise = parser.getboolean('method', 'bool_noise')
	if bool_noise:
		noise_list = np.array([float(i) for i in parser.get('method', 'noise_list').split()])
	else:
		noise_list = np.zeros([len(nu_list)])


	x0 = np.array([float(i) for i in parser.get('sim', 'x0').split()])
	x_min = np.array([float(i) for i in parser.get('sim', 'bnds_min').split()])
	x_max = np.array([float(i) for i in parser.get('sim', 'bnds_max').split()])
	repeat_num = parser.getint('sim', 'repeat_num')
	out_name = parser.get('output', 'out_name')

	### prepare
	Npix = 12 * Nside**2
	art_noise_std = art_fact * np.pi / 10800.0 / np.sqrt(4.0 * np.pi / Npix)
	bnds = tuple(map(tuple,np.vstack((x_min,x_max)).T.tolist()))

	### read_mask
	mask_map = hp.read_map(mask_name, nest=True, verbose=False)
	mask_map = hp.ud_grade(mask_map, nside_out=Nside, order_in='NESTED', order_out='NESTED')

	### read_covariance
	cov = cv.MaskedCovMat(cov_name, mask_map, lense)
	cov_art = art_noise_std**2.0

	if method == 'KK':
		if bool_band:
			maps = mp.MapsKKBand(nu_list, bw_list)
		else:
			maps = mp.MapsKK(nu_list)
	elif method == 'DMM':
		if bool_band:
			maps = mp.MapsDMMBand(nu_list, bw_list)
		else:
			maps = mp.MapsDMM(nu_list)
	elif method == 'ModDMM':
		if bool_band:
			maps = mp.MapsModDMMBand(nu_list, bw_list)
		else:
			maps = mp.MapsModDMM(nu_list)
	elif method == 'CMB':
		maps = mp.MapsCMB(nu_list)
	elif method == 'ModDDMM':
		maps = mp.MapsModDDMM(nu_list)
	else:
		print "no method"
		exit()

	maps.set_isnt_noise(noise_list)
	cov.set_cov_art(cov_art)
	

	### liklihood
	def like(x):
		r = x[0]
		par = x[1:]
		#print x
	
		QU, inoise = maps.compute_map(par)
	
		cov.signal(r)
		cov.noise(inoise) 
	
		l = cov.compute_minos_log_like_masked(QU)
		return l

	
	for i in xrange(repeat_num):
		print i
		### read_map
		maps.read_maps(i, map_name)
		### set noise
		maps.set_noise(i+seed, Npix, art_noise_std)

		res = minimize(like, x0, bounds=bnds, tol=1.0e-6)
	
		if res['success'] == True:
			f = open(out_name, 'a')
			text = ""
			for x in res.x:
				text = text + '%f	'%x
			text = text + '%f	%d\n'%(res['fun'],i)
			f.write(text)
		else:
			i -= 1

if __name__ == '__main__':
	if len(sys.argv) == 1:
		filename = 'config.ini'
	else:
		filename = sys.argv[1]
	parser = read_config(filename)
	main(parser)

