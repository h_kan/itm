#!/usr/bin/env python

import numpy as np
import healpy as hp

h = 6.62607004 * 10**-34
k = 1.38064852 * 10**-23
hk = h/k*10**9

T_cmb=2.725
gamma = hk/T_cmb

class Maps:
	def __init__(self, nu):
		self.nu = nu

	def read_maps(self, num, name):
		data = []
		QU = []
		for n in self.nu:
			file_name = name.replace('[nu]',str(int(n))).replace('[num]',str(int(num+1)))
			d = hp.read_map(file_name, field = (0,1,2), nest = True, verbose = False)
			data.append(d)
			QU.append(np.r_[d[1], d[2]])
		self.data = np.array(data)
		self.QU = np.array(QU)

	def set_noise(self, seed, Npix, art_noise_std):
		np.random.seed(seed)
		self.art_noise = np.random.randn(Npix * 2) * art_noise_std

	def set_isnt_noise(self, inoise):
		self.inoise = inoise

	def compute_map(self, par):
		inoise = self.inoise
		a = self.compute_alphas(par)
		cleaned_map = (self.QU[0] - np.sum(self.QU[1:]*a.reshape([len(self.nu)-1, 1]), axis=0)) / (1.0 - a.sum()) + self.art_noise

		### inst noise
		inst_noise = np.sqrt(inoise[0]**2 + np.sum( (inoise[1:]*a)**2 ))/ (1.0 - a.sum())

		return cleaned_map, inst_noise
	
	def rbrt(self, nu):
		a = (np.exp(gamma*nu) * nu**4) / (np.exp(gamma*nu)-1)**2
		return a
	
	def g(self):
		return 1./self.rbrt(self.nu)
	


class MapsBand(Maps):
	def __init__(self, nu, bw, div=10):
		self.bw = bw
		Maps.__init__(self, nu)
		self.preSimpson(div)

	def preSimpson(self, div=10):
		d_nu = self.nu * self.bw / 2.
		self.hh = d_nu / div
		
		self.x = self.nu.reshape([len(self.nu),1])* np.ones(div*2 + 1) + np.arange(-div, div+1, 1) * self.hh.reshape([len(self.nu),1])
		self.fact = np.array([1]+[4,2]*(div-1)+[4, 1])

	def Simpson(self, func, *args):
		y = func(self.x, *args) * self.fact
		sum_ = np.sum(y,axis=1)
		
		s = self.hh / 3. * sum_
		return  s / (2 * self.nu * (self.bw/2.))

	def g(self):
		return 1./self.Simpson(self.rbrt) 


class MapsCMB(Maps):
	def __init__(self, *args):
		Maps.__init__(self, *args)
	
	def compute_map(self, par):
		inoise = self.inoise
		cleaned_map = self.QU[0] + self.art_noise
		### inst noise
		inst_noise = np.sqrt(inoise[0]**2)

		return cleaned_map, inst_noise


### Delta map method ###
class MapsModDMM(Maps):
	def __init__(self, *args):
		Maps.__init__(self, *args)
		self.g_array = self.g()
	
	def compute_alphas(self, par):
		self.bs = par[0] 
		self.bd = par[1] 
		self.Td = par[2]
		Mat = np.zeros([5, 6])
		g = self.g_array
		As =  g * self.PLs()
		Bs =  g * self.PLlognus()
		Ad =  g * self.MBBd()
		Bd =  g * self.MBBlognud()
		Cd =  g * self.MBBdMBBd()
		Mat[0] = As
		Mat[1] = Bs
		Mat[2] = Ad
		Mat[3] = Bd
		Mat[4] = Cd

		###
		# Mat[:,0] is Vector of CMB band
		# Mat[:,1:] is Matrix of Foreground band
		#    (As0)   (As1 As2 As3 As4) (alpha1)   (0)   
		#    (Bs0)   (Bs1 Bs2 Bs3 Bs4) (alpha2)   (0)
		#    (Ad0) - (Ad1 Ad2 Ad3 Ad4) (alpha3) = (0)  
		#    (Bd0)   (Bd1 Bs2 Bd3 Bd4) (alpha4)   (0)  
		#    (Cd0)   (Cd1 Cs2 Cd3 Cd4) (alpha5)   (0)  
		###
		alphas = np.linalg.solve(Mat[:,1:], Mat[:,0])
		return alphas

	def PL(self, nu, beta):
		return nu**beta
	def PLlognu(self, nu, beta):
		return np.log(nu) * self.PL(nu, beta)
	def MBB(self, nu, beta, T):
		return nu**(beta+3) / (np.exp(hk*nu/T) - 1)
	def MBBlognu(self, nu, beta, T):
		return np.log(nu) * self.MBB(nu, beta, T)
	def MBBdMBB(self, nu, beta, T):
		x = hk*nu/T
		return self.MBB(nu, beta, T) * x * np.exp(x) / (np.exp(x) - 1)

	def PLs(self):
		return self.PL(self.nu, self.bs)
	def PLlognus(self):
		return self.PLlognu(self.nu, self.bs)
	def MBBd(self):
		return self.MBB(self.nu, self.bd, self.Td)
	def MBBlognud(self):
		return self.MBBlognu(self.nu, self.bd, self.Td)
	def MBBdMBBd(self):
		return self.MBBdMBB(self.nu, self.bd, self.Td)
		

class MapsModDMMBand(MapsBand, MapsModDMM):
	def __init__(self, *args):
		MapsBand.__init__(self, *args)
		self.g_array = self.g()

	def PLs(self):
		return self.Simpson(self.PL, self.bs)
	def PLlognus(self):
		return self.Simpson(self.PLlognu, self.bs)
	def MBBd(self):
		return self.Simpson(self.MBB, self.bd, self.Td)
	def MBBlognud(self):
		return self.Simpson(self.MBBlognu, self.bd, self.Td)
	def MBBdMBBd(self):
		return self.Simpson(self.MBBdMBB, self.bd, self.Td)



class MapsKK(Maps):
	def __init__(self, *args):
		Maps.__init__(self, *args)
	
	def compute_alphas(self, par):
		self.a_s = par[0] 
		self.a_d = par[1] 
		#alphas = np.array([a_s, a_d])
		alphas = np.array([self.a_s, self.a_d])
		return alphas

class MapsKKBand(MapsKK, MapsBand):
	def __init__(self, *args):
		MapsBand.__init__(self, *args)

class MapsDMM(Maps):
	def __init__(self, *args):
		Maps.__init__(self, *args)
		self.g_array = self.g()
	
	def compute_alphas(self, par):
		self.bs = par[0] 
		self.bd = par[1] 
		Mat = np.zeros([4, 5])
		g = self.g_array
		As =  g * self.PLs()
		Bs =  g * self.Lognus()
		Ad =  g * self.PLd()
		Bd =  g * self.Lognud()
		Mat[0] = As
		Mat[1] = Bs
		Mat[2] = Ad
		Mat[3] = Bd

		###
		# Mat[:,0] is Vector of CMB band
		# Mat[:,1:] is Matrix of Foreground band
		#    (As0)   (As1 As2 As3 As4) (alpha1)   (0)   
		#    (Bs0) - (Bs1 Bs2 Bs3 Bs4) (alpha2) = (0)
		#    (Ad0)   (Ad1 Ad2 Ad3 Ad4) (alpha3)   (0)  
		#    (Bd0)   (Bd1 Bs2 Bd3 Bd4) (alpha4)   (0)  
		###
		alphas = np.linalg.solve(Mat[:,1:], Mat[:,0])
		return alphas


	def PL(self, nu, beta):
		return nu**beta
	
	def Lognu(self, nu, beta):
		return np.log(nu) * self.PL(nu, beta)

	def PLs(self):
		return self.PL(self.nu, self.bs)
	def PLd(self):
		return self.PL(self.nu, self.bd)
	def Lognus(self):
		return self.Lognu(self.nu, self.bs)
	def Lognud(self):
		return self.Lognu(self.nu, self.bd)

class MapsDMMBand(MapsBand, MapsDMM):
	def __init__(self, *args):
		MapsBand.__init__(self, *args)
		self.g_array = self.g()

	def PLs(self):
		return self.Simpson(self.PL, self.bs)
	def PLd(self):
		return self.Simpson(self.PL, self.bd)
	def Lognus(self):
		return self.Simpson(self.Lognu, self.bs)
	def Lognud(self):
		return self.Simpson(self.Lognu, self.bd)

# double delta map method
class MapsModDDMM(Maps):
	def __init__(self, *args):
		Maps.__init__(self, *args)
		self.g_array = self.g()
	
	def compute_alphas(self, par):
		self.bs = par[0] 
		self.bd = par[1] 
		Mat = np.zeros([4, 5])
		g = self.g_array
		As =  g * self.PLs()
		Bs =  g * self.Lognus()
		Cs =  g * self.LogLognus()
		Ad =  g * self.PLd()
		Bd =  g * self.Lognud()
		Cd =  g * self.LogLognud()
		Mat[0] = As
		Mat[1] = Bs
		Mat[2] = Bs
		Mat[3] = Ad
		Mat[4] = Bd
		Mat[5] = Bd

		###
		# Mat[:,0] is Vector of CMB band
		# Mat[:,1:] is Matrix of Foreground band
		#    (As0)   (As1 As2 As3 As4) (alpha1)   (0)   
		#    (Bs0) - (Bs1 Bs2 Bs3 Bs4) (alpha2) = (0)
		#    (Ad0)   (Ad1 Ad2 Ad3 Ad4) (alpha3)   (0)  
		#    (Bd0)   (Bd1 Bs2 Bd3 Bd4) (alpha4)   (0)  
		###
		alphas = np.linalg.solve(Mat[:,1:], Mat[:,0])
		return alphas


	def PL(self, nu, beta):
		return nu**beta
	
	def Lognu(self, nu, beta):
		return np.log(nu) * self.PL(nu, beta)

	def LogLognu(self, nu, beta):
		return (np.log(nu))**2 * self.PL(nu, beta)

	def PLs(self):
		return self.PL(self.nu, self.bs)
	def PLd(self):
		return self.PL(self.nu, self.bd)
	def Lognus(self):
		return self.Lognu(self.nu, self.bs)
	def Lognud(self):
		return self.Lognu(self.nu, self.bd)
	def LogLognus(self):
		return self.LogLognu(self.nu, self.bs)
	def LogLognud(self):
		return self.LogLognu(self.nu, self.bd)


if __name__ == '__main__':
	nu = np.array([140., 40., 50., 280., 337.])
	m = MapsDMM(nu)
	a = m.compute_alphas(-1.1, 3.53)
	print a

	bw = np.array([0.3,0.3,0.3,0.3,0.3])
	mm = MapsDMMBand(nu, bw)
	aa = mm.compute_alphas(-1.1, 3.53)
	print aa


