#!/usr/bin/env python

import numpy as np
import healpy as hp

class CovMat:
	### read covariance matorix
	def __init__(self, name, lense=False):
		cov_file = np.load(name)
		cov_tensor_EE = cov_file['cov_EE']
		cov_tensor_BB = cov_file['cov_BB']
		if lense:
			cov_scalar_EE = cov_file['cov_lens_EE']
			cov_scalar_BB = cov_file['cov_lens_BB']
		else:
			cov_scalar_EE = cov_file['cov_scalar_EE']
			try:
				cov_scalar_BB = cov_file['cov_scalar_BB']
			except KeyError:
				cov_scalar_BB = np.zeros(cov_scalar_EE.shape)
		self.cov_noise = cov_file['cov_noise']
		self.cov_tensor = cov_tensor_BB + cov_tensor_EE
		self.cov_scalar = cov_scalar_EE + cov_scalar_BB

	def set_cov_art(self, cov_art):
		self.cov_art = cov_art

	def signal(self, r):
		cov_art = self.cov_art
		S = self.cov_tensor * r + self.cov_scalar
		for i in xrange(S.shape[0]):
			S[i][i] += cov_art
		self.S = S

	def noise(self, inoise):
		self.N = self.cov_noise * (inoise / (np.degrees(1) * 60))**2


	def compute_minos_log_like_masked(self, QU):
		A = self.S + self.N
		log_det_A = self._compute_log_det(A)
		v = self._get_vect(QU)
		logL = -0.5 * np.dot(v, np.linalg.solve(A, v)) - 0.5 * log_det_A
		return - logL
	
	def _compute_log_det(self,cov):
		# compute eigenvalues and eigenvector
		w, v = np.linalg.eig(cov)
		log_det = np.sum(np.log(np.real(w)))
		return log_det
	
	def _get_vect(self, map_):
		vect = map_
		return vect


class MaskedCovMat(CovMat):
	def __init__(self, name, mask, lense=False):
		CovMat.__init__(self, name, lense)
		self.masked_cov(mask)

	### mask covariance
	def masked_cov(self, mask):
		self.mask = mask
		self.maskQU = np.r_[mask,mask]
		self.cov_tensor = self._compute_S_masked(self.cov_tensor)
		self.cov_scalar = self._compute_S_masked(self.cov_scalar)
		self.cov_noise = self._compute_N_masked(self.cov_noise)

	def _compute_S_masked(self, S):
		S = self._reorder(S)
		Npix_unmasked = self._N_unmasked(self.maskQU)
		A = S[:Npix_unmasked,:Npix_unmasked]
		return A
	
	def _compute_N_masked(self, N):
		N = self._reorder(N)
		Npix_unmasked = self._N_unmasked(self.maskQU)
		A = N[:Npix_unmasked,:Npix_unmasked]
		D = N[Npix_unmasked:,Npix_unmasked:]
		B = N[:Npix_unmasked,Npix_unmasked:]
		B_t = B.T
		ABDB = A - np.dot(B, np.linalg.solve(D, B_t))
		return ABDB

	def _reorder(self, cov):
		index_unmasked = np.where(self.mask != 0)[0]
		index_masked = np.where(self.mask == 0)[0]
		len_unmasked = len(index_unmasked)
		len_masked = len(index_masked)
		each_size = len(self.mask)
		nmap = 2
		total_size = cov.shape[0]
	
		index_all = np.zeros(total_size, dtype=np.int)
		offset = 0
		for i in xrange(nmap):
			for j in xrange(len_unmasked):
				index_all[len_unmasked*i+j] = index_unmasked[j] + each_size * i
		for i in xrange(nmap):
			for j in xrange(len_masked):
				index_all[len_unmasked*nmap+len_masked*i+j] = index_masked[j] + each_size * i
	
		cov_new = np.ndarray((total_size, total_size), dtype=np.float64)
	
		for i in xrange(total_size):
			index_i = index_all[i]
			cov_new[i, i] = cov[index_i, index_i]
			for j in xrange(i+1, total_size):
				cov_new[i, j] = cov[index_i, index_all[j]]
				cov_new[j, i] = cov_new[i, j]
		return cov_new
	
	def _N_unmasked(self, mask):
		return len(mask) - len(mask[mask==0.0])

	### override function
	def _get_vect(self, map_):
		vect = map_[np.where(self.maskQU != 0)[0]]
		return vect

